<?php
require_once "config/database.php";
require_once "config/fungsi_tanggal.php";
require_once "config/fungsi_rupiah.php";


if (empty($_SESSION['username']) && empty($_SESSION['password'])){
	echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}
else {

//INICIO
	if ($_GET['module'] == 'start') {
		include "modules/start/view.php";
	}

//INSUMOS
	elseif ($_GET['module'] == 'medicines') {
		include "modules/medicines/view.php";
	}

	elseif ($_GET['module'] == 'form_medicines') {
		include "modules/medicines/form.php";
	}
	
//OPERACIONES DE LOS INSUMOS
	elseif ($_GET['module'] == 'medicines_transaction') {
		include "modules/medicines_transaction/view.php";
	}

	elseif ($_GET['module'] == 'form_medicines_transaction') {
		include "modules/medicines_transaction/form.php";
	}

//FACTURACION
	elseif ($_GET['module'] == 'facturacion') {
		include "modules/facturacion/view.php";
	}

	elseif ($_GET['module'] == 'form_facturacion') {
		include "modules/facturacion/form.php";
	}

//ORDENES DE COMPRA
	elseif ($_GET['module'] == 'ordenes_compras') {
		include "modules/ordenes_compras/view.php";
	}

	elseif ($_GET['module'] == 'form_ordenes_compras') {
		include "modules/ordenes_compras/form.php";
	}
	
//REPORTES DE INVENTARIOS
	elseif ($_GET['module'] == 'stock_inventory') {
		include "modules/stock_inventory/view.php";
	}

//REPORTE DE OPERACIONES
	elseif ($_GET['module'] == 'stock_report') {
		include "modules/stock_report/view.php";
	}

//ADMINISTRACION DE USUARIOS
	elseif ($_GET['module'] == 'user') {
		include "modules/user/view.php";
	}


	elseif ($_GET['module'] == 'form_user') {
		include "modules/user/form.php";
	}
// PERFIL
	elseif ($_GET['module'] == 'profile') {
		include "modules/profile/view.php";
		}


	elseif ($_GET['module'] == 'form_profile') {
		include "modules/profile/form.php";
	}
//CONTRASEÑA
	elseif ($_GET['module'] == 'password') {
		include "modules/password/view.php";
	}
}
?>