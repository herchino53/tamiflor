

<?php
session_start();

require_once "../../config/database.php";

if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}

else {


    /// insertar ---- aqui se crea la orden de compra
    if ($_GET['act']=='insert') {
        if (isset($_POST['Guardar']) ) {
            $codigo_transaccion = mysqli_real_escape_string($mysqli, trim($_POST['codigo_transaccion']));
            $proveedor = mysqli_real_escape_string($mysqli, trim($_POST['proveedor']));
            $cirif = mysqli_real_escape_string($mysqli, trim($_POST['cirif']));
			$fecha         = mysqli_real_escape_string($mysqli, trim($_POST['fecha_a']));

            $exp             = explode('-',$fecha);
            $fecha_a   = $exp[2]."-".$exp[1]."-".$exp[0];
            $created_user = $_SESSION['id_user'];
            $updated_user  = $_SESSION['id_user'];

            $query = mysqli_query($mysqli, "
                INSERT INTO ordenes
                (
                    codigo_transaccion,
                    proveedor,
                    cirif,
                    fecha,
                    created_user,
                    updated_user
                ) 
                VALUES('$codigo_transaccion','$proveedor','$cirif','$fecha_a','$created_user', $updated_user)")
                                            or die('Error: '.mysqli_error($mysqli));    

            if ($query) {
                
                $query1 = mysqli_query($mysqli, "UPDATE ordenes SET proveedor        = '$proveedor',
                                                                    fecha        = '$fecha_a'
                                                              WHERE codigo_transaccion   = '$codigo_transaccion'")
                                                or die('Error: '.mysqli_error($mysqli));

               
                if ($query1) {                       
                    
                    header("location: ../../main.php?module=ordenes_compras&alert=1");
                }
            }   
        }   
    }
//actualizar ordenes de compra
    elseif ($_GET['act']=='update') {
        if (isset($_POST['Guardar'])) {
            if (isset($_POST['codigo'])) {
                $codigo  = mysqli_real_escape_string($mysqli, trim($_POST['codigo']));
                $nombre  = mysqli_real_escape_string($mysqli, trim($_POST['nombre']));
                $pcompra = str_replace('.', '', mysqli_real_escape_string($mysqli, trim($_POST['pcompra'])));
                $pventa = str_replace('.', '', mysqli_real_escape_string($mysqli, trim($_POST['pventa'])));
                $unidad     = mysqli_real_escape_string($mysqli, trim($_POST['unidad']));

                $updated_user = $_SESSION['id_user'];

                $query = mysqli_query($mysqli, "UPDATE ordenes SET  nombre       = '$nombre',
                                                                    precio_compra      = '$pcompra',
                                                                    precio_venta      = '$pventa',
                                                                    unidad          = '$unidad',
                                                                    updated_user    = '$updated_user'
                                                              WHERE codigo       = '$codigo'")
                                                or die('error: '.mysqli_error($mysqli));

    
                if ($query) {
                  
                    header("location: ../../main.php?module=ordenes_compras&alert=2");
                }         
            }
        }

        else//if($_GET['Agregar']=='AgregarMed')
        //agregar medicamentos a la orden de compra
            {

                $codigo  = mysqli_real_escape_string($mysqli, trim($_POST['codigo']));
                $cantidad  = mysqli_real_escape_string($mysqli, trim($_POST['cantidad']));
                $fkorden  = mysqli_real_escape_string($mysqli, trim($_POST['codigo_transaccion']));
                

                $query = mysqli_query($mysqli, "
                                            INSERT INTO insumo_orden
                                            (
                                            codigo,
                                            cantidad,
                                            fkorden
                                            ) 
                                            VALUES
                                            (
                                            '$codigo',
                                            '$cantidad',
                                            '$fkorden'                                            
                                            )
                                            ")
                                            or die('error '.mysqli_error($mysqli)); 

    
                if ($query) {
                  
                    //header("location: ../../main.php?module=ordenes_compras&alert=2");
                    header("location: ../../main.php?module=form_ordenes_compras&form=edit&id=".$_POST['codigo_transaccion']."&alert=2");
                }

        }


    }


// eliminar orden de compra
    elseif ($_GET['act']=='delete') {
        if (isset($_GET['id'])) {
            $codigo_transaccion = $_GET['id'];
            //$codigo_transaccion = $_GET['codigo_transaccion'];
            $query = mysqli_query($mysqli, "
                                            DELETE FROM ordenes 
                                            WHERE codigo_transaccion='$codigo_transaccion'
                                            "
                                )
                                            or die('error '.mysqli_error($mysqli));


            if ($query) {
     
                header("location: ../../main.php?module=ordenes_compras&alert=3");

            }
        }
    } 




//eliminar medicamentos de la orden de compra
    elseif ($_GET['act']=='deleteMed') {

        if (isset($_GET['id'])) {

            $codigo = $_GET['id'];
            $orden = $_GET['orden'];

/*echo "<pre>";
var_dump($_GET);
echo "</pre>";            
die();*/
            $query = mysqli_query($mysqli, "
                                            DELETE FROM insumo_orden 
                                            WHERE codigo='$codigo'
                                            AND
                                            fkorden='$orden'
                                            "
                                )
                                            or die('error '.mysqli_error($mysqli));


            if ($query) {
     
                //header("location: ../../main.php?module=ordenes_compras&alert=3");
                header("location: ../../main.php?module=form_ordenes_compras&form=edit&id=".$orden."&alert=2");
            }
        }
    } 

}       
?>