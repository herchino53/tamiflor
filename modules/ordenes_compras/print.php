<?php
session_start();
ob_start();

require_once "../../config/database.php";
include "../../config/fungsi_tanggal.php";
include "../../config/fungsi_rupiah.php";

$hari_ini = date("d-m-Y");

  if (isset($_GET['id'])) {
        $no = 1;
            $query = mysqli_query($mysqli, "SELECT 
                                        ord.codigo_transaccion, 
                                        ord.proveedor,
                                        ord.cirif,
                                        ord.fecha,
                                        io.cantidad,
                                        med.codigo, 
                                        med.nombre, 
                                        med.precio_compra,
                                        med.unidad
                                        FROM ordenes ord
                                        INNER JOIN insumo_orden io ON ord.codigo_transaccion=io.fkorden
                                        INNER JOIN medicamentos med on med.codigo=io.codigo
                                        WHERE ord.codigo_transaccion= '".$_GET['id']."'
                ")
                                            or die('error '.mysqli_error($mysqli));

            $query2 = mysqli_query($mysqli, "SELECT 
                                        codigo_transaccion, 
                                        proveedor,
                                        cirif,
                                        fecha
                                        FROM ordenes
                                        WHERE codigo_transaccion= '".$_GET['id']."'
                ")
                                            or die('error '.mysqli_error($mysqli));

            $data2 = mysqli_fetch_assoc($query2);
    }         
?>

<?php
if (isset($_SESSION['id_user'])){
    
?>

<html xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>ORDEN DE COMPRA</title>
        <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
    </head>
    <body>
        <div>
            <table border=0>
                <tr>
                    <td width="316px;"> 
                    <img style="margin-top:-2px" src="../../assets/img/logo-blue.png" alt="Logo" height="2">       
                    <h4><b>Inversiones TamiFlor, C.A.</b> </h4>
                    <br>
                    RIF J-40411566-8<br>
                    Tlf. 0212 543 05 22 <br><br><br>
                    </td>
                    <td colspan="2" align="right">     
                    ORDEN DE COMPRA #:
                    </td>
                    <td>
<?php
echo $data2['codigo_transaccion'];
?>
                    </td>
 
                </tr>
                <tr>
                    <td>        
                    <b>Se&ntilde;or(es):</b>
                    </td>
                    <td border=1>     
                    <b>Cedula / Rif Proveedor:</b><br>                   
                    </td>
                    <td border=1>
                    <b>Codigo Proveedor:</b><br>
                  
                    </td>
                    <td border=1>
                    <b>Estado:</b><br>
                  
                    </td>
 
                </tr>

                <tr>
                    <td>  
<?php
echo $data2['proveedor'];
?>                          
                    </td>
                    <td border=1>     
<?php
echo $data2['cirif'];
?>                   
                    </td>
                    <td border=1>
                    <b></b><br>
                  
                    </td>
                    <td border=1>
                    <b></b><br>
                  
                    </td>
 
                </tr>                

                <tr>
                    <td colspan="3">        
                        <b>Forma de Pago:</b><br>
                       
                    </td>
                    <td>     
                    <b>Fecha de Pedido:</b><br>
<?php
echo $data2['fecha'];
?>                    
<br><br><br>  
                    </td>
                </tr>                
            </table>

            <table border="1">
                <tr>
                    <td width="10px;">
                    <b>Cant. <br>
                    Pedida:</b>
                    </td>
                    <td width="50px;">
                    <b>Descripci&oacute;n:</b>
                    </td>
                    <td width="10px;">
                    <b>Unidad</b>
                    </td>
                    <td width="60px;">
                    <b>Cant.<br>
                    Recibida:</b>
                    </td>
                    <td width="80px;">
                    <b>Precio:</b>
                    </td>
                    <td width="100px;">
                    <b>TOTAL:</b>
                    </td>
                </tr>
<?php
$TOTAL=0;
$DESCUENTO=0;
$IVA=12;
            while ($data = mysqli_fetch_assoc($query)) {

                $total= $data['cantidad']*$data['precio_compra'];
                $TOTAL=$TOTAL+$total;
    echo "      <tr>
                    <td>".$data['cantidad']."</td>
                    <td>".$data['nombre']."</td>
                    <td>".$data['unidad']."</td>
                    <td>                   </td>
                    <td>".$data['precio_compra']." Bs.</td>
                    <td>".$total." Bs</td>
                </tr>";

            $no++;

            }
?>

                <tr>
                    <td colspan="3" rowspan="4" border=0>
                    <b>DOCUMENTOS DE DESPACHO:</b><br><br>
                        Original y Dos (2) Copias de la Factura Comercial
                        en la cual nos deben indicar <br>
                        su n&uacute;mero de Identificaci&oacute;n y nuestro n&uacute;mero de Orden de Compra.    
                    </td>
                    
                    
                    <td colspan="2" border=0 align="right">
                    SUBTOTAL:
                    </td>
                    <td border=1>
<?php
echo $TOTAL." Bs.";
?>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 border=0 align="right">
                    DESCUENTO:
                    </td>
                    <td border=1>
<?php
echo $DESCUENTO." Bs.";
?>                    
                    </td>
                </tr>
                <tr>
                    <td colspan=2 border=0 align="right">
                    IVA (12%):
                    </td>
                    <td border=1>
<?php
$iva=$TOTAL*$IVA/100;
$suma=$TOTAL-$DESCUENTO+$iva;
echo $iva." Bs.";
?>                    
                    </td>
                </tr>  
                <tr>
                    <td colspan=2 border=0 align="right">
                    <b>TOTAL:</b>
                    </td>
                    <td border=1>
<b>
<?php
echo $suma." Bs.";
?> 
</b>       
                    </td>
                </tr>                              

                <tr>
                    <td colspan=6 border=1>
                    <b>OBSERVACIONES PARA EL PROVEEDOR:</b>
                    <br><br><br><br><br><br>
                    </td>
                </tr>  
                </table>

                <table align="center">
                <tr>
                    <td border=0 align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        AUTORIZADO POR(Nombre y Firma): 
                        </b>
                        <br><br>
                    </td>
                    <td border=0 width="200px;" align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        PROVEEDOR:
                        </b>
                        <br>
<?php
echo $data2['proveedor'];
?>
<br>
<?php
echo $data2['cirif'];
?>    
                        <br><br>
                    </td>
                    <td border=0 align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        SOLICITADO POR(Nombre y Firma):
                        </b>
                        <br>
<?php
echo $_SESSION['name_user'];
?>                        
                        
                        <br><br>
                    </td>
                    
                </tr> 

            </table>
        </div>
    </body>
</html>


<?php
}else{
    echo "no tene sesion para Imprimir Ordenes de Compra";
    die();
}
?>


<?php
$filename="tamiflor-ORDEN DE COMPRA-".$_GET[id].".pdf"; 
//==========================================================================================================
$content = ob_get_clean();
$content = '<page style="font-family: freeserif">'.($content).'</page>';

require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P','F4','en', false, 'ISO-8859-15',array(10, 10, 10, 10));
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>