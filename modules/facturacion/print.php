<?php
session_start();
ob_start();
require_once "../phpqrcode/qrlib.php";//generador de codigo QR
require_once "../../config/database.php";
include "../../config/fungsi_tanggal.php";
include "../../config/fungsi_rupiah.php";

$hari_ini = date("d-m-Y");
  if (isset($_GET['id'])) {
        $no = 1;
            $query = mysqli_query($mysqli, "SELECT 
                                        fac.codigo_transaccion, 
                                        fac.cliente,
                                        fac.cirif,
                                        fac.fecha,
                                        ifac.cantidad,
                                        med.codigo, 
                                        med.nombre, 
                                        med.precio_compra,
                                        med.unidad
                                        FROM facturas fac
                                        INNER JOIN insumo_factura ifac ON fac.codigo_transaccion=ifac.fkfactura
                                        INNER JOIN medicamentos med on med.codigo=ifac.codigo
                                        WHERE fac.codigo_transaccion= '".$_GET['id']."'
                ")
                                            or die('error '.mysqli_error($mysqli));

            $query2 = mysqli_query($mysqli, "SELECT 
                                        codigo_transaccion, 
                                        cliente,
                                        cirif,
                                        fecha
                                        FROM facturas
                                        WHERE codigo_transaccion= '".$_GET['id']."'
                ")
                                            or die('error '.mysqli_error($mysqli));
            $data2 = mysqli_fetch_assoc($query2);
    }        
?>

<?php
if (isset($_SESSION['id_user'])){

///generacion del CODIGO QR
$dir = 'temp/';
if(!file_exists($dir)){
 mkdir ($dir);// si la carpeta no existe la crea
}

$filename = $dir.'test.png';
$tamano = 4; // lo que le vamos a asignar
$level = 'M';// tipo de presicion
$frameSize ='0';//marco que va a tener
$contenido=$data2['codigo_transaccion']."-".$data2['cirif']."-".$_SESSION['id_user'];//lo que va a mostrar
QRcode::png($contenido, $filename, $level, $tamano, $frameSize);
// fin de la generacion del CODIGO QR

?>




<html xmlns="http://www.w3.org/1999/xhtml"> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>FACTURA</title>
        <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
    </head>
    <body>
       
      
        <div>
            <table border=0>
                <tr>
                    <td width="316px;">
                    <img style="margin-top:-2px" src="../../assets/img/logo-blue.png" alt="Logo" height="2">
                    <h4><b>Inversiones TamiFlor, C.A.</b> </h4>
                    <br>
                    RIF J-40411566-8<br>
                    Tlf. 0212 543 05 22 <br><br><br>
                    </td>
                    <td colspan="2" align="right">     
                    
<?php

echo ('<br><img style="margin-top:-2px" src='.$filename.'>');
?>

                    </td>
                    <td>
                        FACTURA #:
<?php
echo $data2['codigo_transaccion'];
?>

                    </td>
 
                </tr>
                <tr>
                    <td>        
                    <b>Se&ntilde;or(es):</b>
                    </td>
                    <td border=1>     
                    <b>Cedula / Rif Cliente:</b><br>                  
                    </td>
                    <td border=1>
                    <b>Codigo Cliente:</b><br>
                    </td>
                    <td border=1>
                    <b>Estado:</b><br>
                    </td>
                </tr>
                <tr>
                    <td>  
<?php
echo $data2['cliente'];
?>                          
                    </td>
                    <td border=1>     
<?php
echo $data2['cirif'];
?>                    
                    </td>
                    <td border=1>
                    <b></b><br>
                    </td>
                    <td border=1>
                    <b></b><br>
                    </td>
 
                </tr>                

                <tr>
                    <td colspan="3">        
                        <b>Forma de Pago:</b><br>
                    </td>
                    <td>     
                    <b>Fecha de Pedido:</b><br>
<?php
echo $data2['fecha'];
?>                    
<br><br><br>  
                    </td>
                </tr>                
            </table>

            <table border="1">
                <tr>
                    <td width="10px;">
                    <b>Cant. <br>
                    Pedida:</b>
                    </td>
                    <td width="50px;">
                    <b>Descripci&oacute;n:</b>
                    </td>
                    <td width="10px;">
                    <b>Unidad</b>
                    </td>
                    <td width="60px;">
                    <b>Cant.<br>
                    Recibida:</b>
                    </td>
                    <td width="80px;">
                    <b>Precio:</b>
                    </td>
                    <td width="100px;">
                    <b>TOTAL:</b>
                    </td>
                </tr>
<?php
$TOTAL=0;
$DESCUENTO=0;
$IVA=12;
            while ($data = mysqli_fetch_assoc($query)) {

                $total= $data['cantidad']*$data['precio_compra'];
                $TOTAL=$TOTAL+$total;
    echo "      <tr>
                    <td>".$data['cantidad']."</td>
                    <td>".$data['nombre']."</td>
                    <td>".$data['unidad']."</td>
                    <td>                   </td>
                    <td>".$data['precio_compra']." Bs.</td>
                    <td>".$total." Bs</td>
                </tr>";

            $no++;

            }
?>
                <tr>
                    <td colspan="3" rowspan="4" border=0>
                    <b>DOCUMENTOS DE DESPACHO:</b><br><br>
                        Original y Dos (2) Copias de la Factura Comercial
                        en la cual nos deben indicar <br>
                        su n&uacute;mero de Identificaci&oacute;n y nuestro n&uacute;mero de Orden de Compra.    
                    </td>
                    
                    
                    <td colspan="2" border=0 align="right">
                    SUBTOTAL:
                    </td>
                    <td border=1>
<?php
echo $TOTAL." Bs.";
?>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 border=0 align="right">
                    DESCUENTO:
                    </td>
                    <td border=1>
<?php
echo $DESCUENTO." Bs.";
?>                    
                    </td>
                </tr>
                <tr>
                    <td colspan=2 border=0 align="right">
                    IVA (12%):
                    </td>
                    <td border=1>
<?php
$iva=$TOTAL*$IVA/100;
$suma=$TOTAL-$DESCUENTO+$iva;
echo $iva." Bs.";
?>                    
                    </td>
                </tr>  
                <tr>
                    <td colspan=2 border=0 align="right">
                    <b>TOTAL:</b>
                    </td>
                    <td border=1>
<b>
<?php
echo $suma." Bs.";
?> 
</b>       
                    </td>
                </tr>                              
                <tr>
                    <td colspan=6 border=1>
                    <b>OBSERVACIONES PARA EL CLIENTE:</b>
                    <br><br><br><br><br><br>
                    </td>
                </tr>  
                </table>

                <table align="center">
                <tr>
                    <td border=0 align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        AUTORIZADO POR(Nombre y Firma): 
                        </b>
                        <br><br>
                    </td>
                    <td border=0 width="200px;" align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        CLIENTE:
                        </b>
                        <br>
<?php
echo $data2['cliente'];
?>
<br>
<?php
echo $data2['cirif'];
?>   
                        
                        <br><br>
                    </td>
                    <td border=0 align="center">
                        <br><br><br><br><br><br><br><br>
                    <hr>
                        <br>
                        <b>
                        SOLICITADO POR(Nombre y Firma):
                        </b>
                        <br>
<?php
echo $_SESSION['name_user'];
?>                        
                        
                        <br><br>
                    </td>
                </tr> 
            </table>
        </div>
    </body>
</html>

<?php
}else{
    echo "no tene sesion para Imprimir Facturas";
    die();
}
?>
<?php
$filename="tamiflor-FACTURA-".$_GET[id].".pdf"; 
//==========================================================================================================
$content = ob_get_clean();
$content = '<page style="font-family: freeserif">'.($content).'</page>';

require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
try
{
    $html2pdf = new HTML2PDF('P','F4','en', false, 'ISO-8859-15',array(10, 10, 10, 10));
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>