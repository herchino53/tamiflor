
<script type="text/javascript">
  function tampil_obat(input){
    var num = input.value;

    $.post("modules/facturacion/facturas.php", {
      dataidobat: num,
    }, function(response) {      
      $('#stok').html(response)

      document.getElementById('jumlah_masuk').focus();
    });
  }

  function cek_jumlah_masuk(input) {
    jml = document.formObatMasuk.jumlah_masuk.value;
    var jumlah = eval(jml);
    if(jumlah < 1){
      alert('Revisar valor CANTIDAD !!');
      input.value = input.value.substring(0,input.value.length-1);
    }
  }


  function hitung_total_stok() {
    bil1 = document.formObatMasuk.stok.value;
    bil2 = document.formObatMasuk.jumlah_masuk.value;


	  tt = document.formObatMasuk.transaccion.value;
	
    if (bil2 == "") {
      var hasil = "";
    }
    else {
    var salida = eval(bil1) - eval(bil2);
	  var hasil = eval(bil1) + eval(bil2);
    }

	if (tt=="Salida"){
		document.formObatMasuk.total_stok.value = (salida);//RESTA
if(document.formObatMasuk.total_stok.value<0){
      alert('Revisar valor CANTIDAD, Total Stock es NEGATIVA !!');
}


	}	else {
		document.formObatMasuk.total_stok.value = (hasil);//SUMA
    if(document.formObatMasuk.total_stok.value<0){
      alert('Revisar valor CANTIDAD, Total Stock es NEGATIVA !!');
}
	} 
    



  }
</script>

<?php  
//////////agregar
if ($_GET['form']=='add') { ?> 

  <section class="content-header">
    <h1>
      <i class="fa fa-edit icon-title"></i> Facturas
    </h1>
    <ol class="breadcrumb">
      <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
      <li><a href="?module=facturacion"> Facturas </a></li>
      <li class="active"> Agregar </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <!-- form start -->
          <form role="form" class="form-horizontal" action="modules/facturacion/proses.php?act=insert" method="POST" name="formObatMasuk">
            <div class="box-body">
              <?php  
            
              $query_id = mysqli_query($mysqli, "
                                                SELECT 
                                                    RIGHT(codigo_transaccion,7) as codigo 
                                                FROM facturas
                                                ORDER BY codigo_transaccion DESC LIMIT 1")
                                                or die('Error : '.mysqli_error($mysqli));

              $count = mysqli_num_rows($query_id);

              if ($count <> 0) {
                 
                  $data_id = mysqli_fetch_assoc($query_id);
                  $codigo    = $data_id['codigo']+1;
              } else {
                  $codigo = 1;
              }

             
              $tahun          = date("Y");
              $buat_id        = str_pad($codigo, 7, "0", STR_PAD_LEFT);
              $codigo_transaccion = "FC-$tahun-$buat_id";
              ?>

              <div class="form-group">
                <label class="col-sm-2 control-label">Codigo de Transacción </label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="codigo_transaccion" value="<?php echo $codigo_transaccion; ?>" readonly required>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Cliente </label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="cliente" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">CI-RIF </label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="cirif" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Fecha</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" name="fecha_a" autocomplete="off" value="<?php echo date("d-m-Y"); ?>" required>
                </div>
              </div>


            </div><!-- /.box body -->

            <div class="box-footer">
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Guardar">
                  <a href="?module=ordenes_compras" class="btn btn-default btn-reset">Cancelar</a>
                </div>
              </div>
            </div><!-- /.box footer -->
          </form>
        </div><!-- /.box -->
      </div><!--/.col -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
<?php
}
//////////agregar







//////editar
//// modificar factura
elseif ($_GET['form']=='edit') { 
  if (isset($_GET['id'])) {

      $query = mysqli_query($mysqli, "  SELECT 
                                            codigo_transaccion,
                                            cliente,
                                            cirif,
                                            fecha

                                        FROM facturas 
                                        WHERE codigo_transaccion='$_GET[id]'
                                      ") 
                                      or die('error: '.mysqli_error($mysqli));
      $data  = mysqli_fetch_assoc($query);


      //var_dump($data);die();
    }
?>

  <section class="content-header">
    <h1>
      <i class="fa fa-edit icon-title"></i> Modificar Factura
    </h1>
    <ol class="breadcrumb">
      <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
      <li><a href="?module=facturacion"> Facturas</a></li>
      <li class="active"> Modificar </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <!-- form start -->
          <form role="form" class="form-horizontal" action="modules/facturacion/proses.php?act=update" method="POST">
            <div class="box-body">
              

<?php

?>

<div class="col-sm-12">
  <a class="btn btn-primary btn-social pull-right" href="modules/facturacion/print.php?act=imprimir&id=<?php echo $_GET['id'];?>" target="_blank">
    <i class="fa fa-print"></i> Imprimir
  </a>
</div>
        


              <div class="form-group">
                <label class="col-sm-2 control-label">Factura:</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="codigo_transaccion" value="<?php echo $data['codigo_transaccion']; ?>" 
                  readonly 
                  required>
                </div>

                <label class="col-sm-1 control-label">Cliente: </label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="cliente" value="<?php echo $data['cliente']; 
                  ?>" 
                  readonly 
                  >

                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Fecha: </label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" data-date-format="dd-mm-yyyy" name="fecha" value="<?php echo $data['fecha']; 
                  ?>" 
                  readonly 
                  >
                </div>
                <label class="col-sm-1 control-label">CI-RIF: </label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" name="cliente" value="<?php echo $data['cirif']; 
                  ?>" 
                  readonly 
                  >

                </div>
              </div>

<!-- crear factura -->
            <div class="box-footer">
              <div class="form-group">
                <div class="col-sm-offset-11 col-sm-10">
                  <!-- <input type="submit" class="btn btn-success btn-submit"  name="Guardar"  title="Guardar factura" value="Guardar"> -->
                  <a href="?module=facturacion" class="btn-sm btn-info btn-reset">Volver</a>
                </div>
              </div>
            </div><!-- /.box footer -->


<hr>


<!--////////agregar insumos a la factura ///////-->
    <h4>
      <i class="fa fa-plus-square icon-title"></i>Agregar Insumos de la Factura
    </h4>
              <div class="form-group">
                <label class="col-sm-2 control-label">Cantidad:</label>
                <div class="col-sm-2">
                  <input type="text" class="form-control" name="cantidad" value="" 
                  required>
                </div>
 
                <label class="col-sm-1 control-label">Insumo:</label>
                <div class="col-sm-5">
                  <select class="chosen-select" name="codigo" data-placeholder="-- Seleccionar Insumo --" onchange="tampil_obat(this)" autocomplete="off" required>
                    <option value=""></option>
                    <?php
                      $query_obat = mysqli_query($mysqli, " SELECT 
                                                                codigo, nombre 
                                                            FROM Medicamentos 
                                                            ORDER BY nombre ASC")
                                                            or die('error '.mysqli_error($mysqli));
                      while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                        echo"<option value=\"$data_obat[codigo]\"> $data_obat[codigo] | $data_obat[nombre] </option>";
                      }
                    ?>
                  </select>
                </div>

              </div>

              <span id='stok'>
              <div class="form-group">
                <label class="col-sm-2 control-label">Stock</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="stok" name="stock" readonly required>
                </div>
              </div>
              </span>

            </div><!-- /.box body -->


<!-- agregar insumo medico a la orden -->

              <div class="form-group">
                <div class="col-sm-offset-10 col-sm-10">
                  <input type="submit" class="btn btn-success btn-submit" title="Agregar Insumo a la Factura" name="AgregarMed" value="Agregar">
                </div>
              </div>
            </div>

<!--      agregar insumo a la factura    -->

<hr>

<!--      lista de medicamentos agregados a la factura    -->

         
          <table id="dataTables1" class="table table-bordered table-striped table-hover">
           
            <thead>
              <tr>
                <th class="center">No.</th>
                <th class="center">Cantidad</th>
                <th class="center">Insumo</th>
                <th></th>
              </tr>
            </thead>
         
            <tbody>
            <?php  
            $no = 1;
           
            $query = mysqli_query($mysqli, "
                                            SELECT 
                                            ifac.codigo,
                                            med.nombre,
                                            ifac.cantidad,
                                            ifac.fkfactura
                                            FROM insumo_factura ifac
                                            INNER JOIN medicamentos med on med.codigo=ifac.codigo
                                            WHERE fkfactura= '".$data['codigo_transaccion']."'
                                            ORDER BY fkfactura DESC")
                                            or die('error '.mysqli_error($mysqli));

           
            while ($data = mysqli_fetch_assoc($query)) { 
/*             
echo "<pre>";
var_dump($data);
echo "</pre>";*/
              echo "<tr>
                      <td width='30' class='center'>$no</td>

                      <td width='100' class='center'>$data[cantidad]</td>
                      <td width='100' class='center'>$data[nombre]</td>
                      <td class='center' width='20'>
                        <div>"
                          ;
            ?>
                          <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-danger btn-sm" href="modules/facturacion/proses.php?act=deleteMed&id=<?php echo $data['codigo'];?>&factura=<?php echo $data['fkfactura'];?>"
                          onclick="return confirm('estas seguro de eliminar insumo <?php echo $data['nombre']; ?> de la factura ?');">
                              <i style="color:#fff" class="glyphicon glyphicon-trash"></i>
                          </a>
            <?php
              echo "    </div>
                      </td> 
                    </tr>";
              $no++;
            }
            ?>
            </tbody>
          </table>

<!--      lista de medicamentos agregados a la factura    -->


          </form>
        </div><!-- /.box -->
      </div><!--/.col -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
<?php
}



?>