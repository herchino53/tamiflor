<ul class="sidebar-menu">
        <li class="header">MENU</li>

	<?php 

	if ($_GET["module"]=="start") { 
		$active_home="active";
	} else {
		$active_home="";
	}
	?>





		<li class="<?php echo $active_home;?>">
			<a href="?module=start"><i class="fa fa-home"></i> Inicio </a>
	  	</li>
	<?php

//////////////// INSUMOS


if ($_SESSION['permisos_acceso']=='Administrador' || $_SESSION['permisos_acceso']=='Gerente') { 

  if ($_GET["module"]=="medicines" || $_GET["module"]=="form_medicines") { ?>
    <li class="active">
      <a href="?module=medicines"><i class="fa fa-folder"></i> Insumos </a>
      </li>
  <?php
  }  else { ?>
    <li>
      <a href="?module=medicines"><i class="fa fa-folder"></i> Insumos </a>
      </li>
  <?php
  }



}


//////REGISTRO DE INSUMOS

if ($_SESSION['permisos_acceso']=='Administrador' || $_SESSION['permisos_acceso']=='Gerente' || $_SESSION['permisos_acceso']=='Vendedor' || $_SESSION['permisos_acceso']=='Almacenista') { 

  if ($_GET["module"]=="medicines_transaction" || $_GET["module"]=="form_medicines_transaction") { ?>
    <li class="active">
      <a href="?module=medicines_transaction"><i class="fa fa-exchange"></i> Transacción de Insumo </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=medicines_transaction"><i class="fa fa-exchange"></i> Transacción de Insumo </a>
      </li>
  <?php
  }


	if ($_GET["module"]=="stock_inventory") { ?>
		<li class="active treeview">
          	<a href="javascript:void(0);">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li class="active"><a href="?module=stock_inventory"><i class="fa fa-circle-o"></i> Inventario </a></li>
        		<li><a href="?module=stock_report"><i class="fa fa-exchange"></i> Transacción de Insumo</a></li>
      		</ul>
    	</li>
    <?php
	}

}


/**/
//////FACTURACION
if ($_SESSION['permisos_acceso']=='Administrador' || $_SESSION['permisos_acceso']=='Gerente' || $_SESSION['permisos_acceso']=='Almacenista'|| $_SESSION['permisos_acceso']=='Vendedor') { 

  if ($_GET["module"]=="facturacion" || $_GET["module"]=="form_facturacion") { ?>
    <li class="active">
      <a href="?module=facturacion"><i class="fa fa-clipboard"></i> Facturación </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=facturacion"><i class="fa fa-clipboard"></i> Facturación </a>
      </li>
  <?php
  }

}
/**/


/**/
//////ORDENES DE COMPRA
if ($_SESSION['permisos_acceso']=='Administrador' || $_SESSION['permisos_acceso']=='Gerente' || $_SESSION['permisos_acceso']=='Almacenista') { 

  if ($_GET["module"]=="ordenes_compras" || $_GET["module"]=="form_ordenes_compras") { ?>
    <li class="active">
      <a href="?module=ordenes_compras"><i class="fa fa-clone"></i> Ordenes de Compras </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=ordenes_compras"><i class="fa fa-clone"></i> Ordenes de Compra </a>
      </li>
  <?php
  }

}
/**/






//////////////REPORTES
if ($_SESSION['permisos_acceso']=='Administrador' || $_SESSION['permisos_acceso']=='Gerente') { 

	if ($_GET["module"]=="stock_report") { ?>
		<li class="active treeview">
          	<a href="javascript:void(0);">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li><a href="?module=stock_inventory"><i class="fa fa-circle-o"></i> Inventario </a></li>
        		<li class="active"><a href="?module=stock_report"><i class="fa fa-circle-o"></i> Movimiento de Insumos </a></li>
      		</ul>
    	</li>
    <?php
	}

	else { ?>
		<li class="treeview">
          	<a href="javascript:void(0);">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li><a href="?module=stock_inventory"><i class="fa fa-circle-o"></i> Inventario </a></li>
        		<li><a href="?module=stock_report"><i class="fa fa-circle-o"></i> Movimiento de Insumos </a></li>
      		</ul>
    	</li>
    <?php
	}


}





////ADMINISTRADOR DE USUARIOS
if ($_SESSION['permisos_acceso']=='Administrador') { 

	if ($_SESSION['permisos_acceso']=='Gerente' && ($_GET["module"]=="user" || $_GET["module"]=="form_user")) { ?>
		<li class="active">
			<a href="?module=user"><i class="fa fa-user"></i> Administrar usuarios</a>
	  	</li>
	<?php
	}

	else { ?>
		<li>
			<a href="?module=user"><i class="fa fa-user"></i> Administrar usuarios</a>
	  	</li>
	<?php
	}

}





/////  CAMBIAR CONTRASEÑA 

	if ($_GET["module"]=="password") { ?>
		<li class="active">
			<a href="?module=password"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php	}	else { ?>
		<li>
			<a href="?module=password"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	?>
	</ul>

