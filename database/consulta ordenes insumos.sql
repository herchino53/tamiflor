SELECT 
io.codigo,
med.nombre,
io.cantidad,
io.fkorden
FROM insumo_orden io
INNER JOIN medicamentos med on med.codigo=io.codigo
WHERE fkorden= 'OC-2018-0000002'
ORDER BY fkorden DESC;