-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-01-2018 a las 01:14:44
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `medisys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentos`
--

CREATE TABLE IF NOT EXISTS `medicamentos` (
  `codigo` varchar(7) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(3) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo`),
  KEY `created_user` (`created_user`),
  KEY `updated_user` (`updated_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `medicamentos`
--

INSERT INTO `medicamentos` (`codigo`, `nombre`, `precio_compra`, `precio_venta`, `unidad`, `stock`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
('B000362', 'Dulvanex', 50, 75, 'cajas', 10, 1, '2017-07-24 16:43:20', 1, '2017-07-26 02:09:06'),
('B000363', 'Controlip', 12, 50, 'cajas', 10, 1, '2017-07-24 16:56:58', 1, '2017-07-26 02:09:28'),
('B000364', 'Quetiazic', 25, 50, 'cajas', 30, 1, '2017-07-25 02:59:48', 1, '2017-07-26 02:09:36'),
('B000365', 'Atamel', 20, 100, 'caja', 18, 1, '2018-01-14 22:02:46', 1, '2018-01-14 23:30:20'),
('B000366', 'Guantes', 10, 20, 'unidad', 0, 1, '2018-01-14 22:44:18', 1, '2018-01-14 22:44:34'),
('B000367', 'Geringas', 10, 15, 'unidad', 0, 1, '2018-01-14 22:44:55', 1, '2018-01-14 22:44:55'),
('B000368', 'colector de orina', 10, 15, 'caja', 20, 1, '2018-01-14 22:48:09', 1, '2018-01-14 23:41:57'),
('B000369', 'Avodar', 10, 15, 'caja', 48, 1, '2018-01-14 23:08:20', 1, '2018-01-14 23:40:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion_medicamentos`
--

CREATE TABLE IF NOT EXISTS `transaccion_medicamentos` (
  `codigo_transaccion` varchar(15) NOT NULL,
  `fecha` date NOT NULL,
  `codigo` varchar(7) NOT NULL,
  `numero` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_transaccion` varchar(50) NOT NULL,
  `cedularif` varchar(10) NOT NULL,
  PRIMARY KEY (`codigo_transaccion`),
  KEY `id_barang` (`codigo`),
  KEY `created_user` (`created_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transaccion_medicamentos`
--

INSERT INTO `transaccion_medicamentos` (`codigo_transaccion`, `fecha`, `codigo`, `numero`, `created_user`, `created_date`, `tipo_transaccion`, `cedularif`) VALUES
('16576717', '2018-01-15', 'B000365', 1, 1, '2018-01-14 23:30:20', 'Salida', ''),
('TM-2017-0000001', '2017-07-26', 'B000362', 5, 1, '2017-07-26 02:09:06', 'Entrada', ''),
('TM-2017-0000002', '2017-07-26', 'B000363', 10, 1, '2017-07-26 02:09:28', 'Entrada', ''),
('TM-2017-0000003', '2017-07-26', 'B000364', 5, 1, '2017-07-26 02:09:36', 'Salida', ''),
('TM-2018-0000004', '2018-01-14', 'B000365', 20, 1, '2018-01-14 22:03:49', 'Entrada', ''),
('TM-2018-0000005', '2018-01-14', 'B000365', 1, 1, '2018-01-14 22:04:05', 'Salida', ''),
('TM-2018-0000006', '2018-01-15', 'B000369', 50, 1, '2018-01-14 23:08:43', 'Entrada', ''),
('TM-2018-0000007', '2018-01-15', 'B000369', 1, 1, '2018-01-14 23:35:59', 'Salida', ''),
('TM-2018-0000008', '2018-01-15', 'B000369', 1, 1, '2018-01-14 23:40:29', 'Salida', 'V-16576717'),
('TM-2018-0000009', '2018-01-15', 'B000368', 20, 1, '2018-01-14 23:41:57', 'Entrada', 'J-28055664');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `permisos_acceso` enum('Administrador','Gerente','Almacenista','Asistente','Vendedor','Chofer','Secretaria') NOT NULL,
  `status` enum('activo','bloqueado') NOT NULL DEFAULT 'activo',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  KEY `level` (`permisos_acceso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_user`, `username`, `name_user`, `password`, `email`, `telefono`, `foto`, `permisos_acceso`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Hernan Abreu', '21232f297a57a5a743894a0e4a801fc3', 'herchino53@gmail.com', '04141140982', '13298087_1559104587724476_1832060351_a.jpg', 'Administrador', 'activo', '2017-04-01 08:15:15', '2018-01-14 07:09:24'),
(2, 'Mariarles', 'Mariarles', '202cb962ac59075b964b07152d234b70', 'juab@juan.com', '12000', '1015957_683419268339673_1552828715_o.jpg', 'Almacenista', 'activo', '2017-07-25 22:34:03', '2018-01-14 07:15:59'),
(3, 'miguel', 'Miguel Lopez', '202cb962ac59075b964b07152d234b70', '', '', NULL, 'Gerente', 'activo', '2018-01-14 06:40:06', '2018-01-14 07:16:30'),
(4, 'flor', 'Flor Olmos', '202cb962ac59075b964b07152d234b70', '', '', NULL, 'Vendedor', 'activo', '2018-01-14 06:40:58', '2018-01-14 14:51:02');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `medicamentos`
--
ALTER TABLE `medicamentos`
  ADD CONSTRAINT `medicamentos_ibfk_1` FOREIGN KEY (`created_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `medicamentos_ibfk_2` FOREIGN KEY (`updated_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `transaccion_medicamentos`
--
ALTER TABLE `transaccion_medicamentos`
  ADD CONSTRAINT `transaccion_medicamentos_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `medicamentos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaccion_medicamentos_ibfk_2` FOREIGN KEY (`created_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
